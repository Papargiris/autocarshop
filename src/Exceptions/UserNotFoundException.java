/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author User
 */
public class UserNotFoundException extends CrudExceptions {
    
    
    @Override
    public String getMessage() {
        String message="User with such id does not exist";
        return message; 
    }
}
