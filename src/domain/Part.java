/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author User
 */
public class Part {

    private int id;
    private String name;
    private PartTypeEnum type;
    private float cost;

    public Part() {

    }

    public Part(int id, String name, PartTypeEnum type, float cost) {
        this.id = id;
        this.cost = cost;
        this.name = name;
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(PartTypeEnum type) {
        this.type = type;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PartTypeEnum getType() {
        return type;
    }

    public float getCost() {
        return cost;
    }

}
