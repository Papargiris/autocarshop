/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Deadpool
 */
import domain.User;
import java.util.ArrayList;
import java.util.Date;

public class Vehicle {

    private int Id;
    private String brand;
    private String model;
    private Date CreationDate;
    private String color;
    private int price;
    private int ownerID;
    private ArrayList<Integer> repairs;

    public int getOwnerID() {
        return ownerID;
    }

    public ArrayList<Integer> getRepairs() {
        return repairs;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public Vehicle() {

    }

    public Vehicle(int Id, String model, Date CreationDate, String color, int price, int ownerID, String brand) {
        this.Id = Id;
        this.model = model;
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.ownerID = ownerID;
    }

    public int getId() {
        return Id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Date getCreationDate() {
        return CreationDate;
    }

    public String getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    public int getOwner() {
        return ownerID;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCreationDate(Date CreationDate) {
        this.CreationDate = CreationDate;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setOwner(int ownerID) {
        this.ownerID = ownerID;
    }

    @Override
    public String toString() {
        String vehicle = "Vehicle Brand:" + this.getBrand() + ",model:" + this.getModel() + ",vehicle id:" + this.getId();
        return vehicle;
    }
}
