/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import domain.RepairTypeEnum;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author User
 */
public class Repair {

    private int id;
    private Date repairDate;
    private RepairTypeEnum Status;
    private int vehicleId;
    private float ServiceCost;
    private ArrayList<Integer> repairParts;

    public Repair() {
    }

    public Repair(int id, RepairTypeEnum Status, int vehicleId, float cost, Date repairDate) {
        this.id = id;

        this.repairDate = repairDate;
        this.Status = Status;
        this.vehicleId = vehicleId;
        this.ServiceCost = cost;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRepairDate(Date repairDate) {
        this.repairDate = repairDate;
    }

    public void setStatus(RepairTypeEnum Status) {
        this.Status = Status;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public void setCost(float cost) {
        this.ServiceCost = ServiceCost;
    }

    public int getId() {
        return id;
    }

    public Date getRepairDate() {
        return repairDate;
    }

    public RepairTypeEnum getStatus() {
        return Status;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public float getCost() {
        return ServiceCost;
    }

    public float getServiceCost() {
        return ServiceCost;
    }

    public ArrayList<Integer> getRepairParts() {
        return repairParts;
    }

    public void setServiceCost(int ServiceCost) {
        this.ServiceCost = ServiceCost;
    }

    public void setRepairParts(ArrayList<Integer> repairParts) {
        this.repairParts = repairParts;
    }

    @Override
    public String toString() {
        String pattern = "MM/dd/yyyy";
        DateFormat df = new SimpleDateFormat(pattern);
        Date d = this.getRepairDate();
        String date = df.format(d);
        String user = "Repair: " + this.id + " date:" + date + " status: " + this.getStatus();
        return user;
    }

}
