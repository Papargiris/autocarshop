/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import domain.UserTypeEnum;
import java.util.ArrayList;

/**
 *
 * @author Deadpool
 */
import java.util.ArrayList;
import java.util.Objects;

public class User {

    public static int counter = 0;
    private String Email;
    private String FirstName;
    private String LastName;
    private String Address;
    private ArrayList<Integer> garage;
    private int afm;
    private String password;
    private UserTypeEnum type;

    public User() {
    }

    public User(int afm, String Email, String FirstName, String LastName, UserTypeEnum type, String Address, String password) {
        this.afm = afm;
        this.Email = Email;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Address = Address;
        this.type = type;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return Email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getAddress() {
        return Address;
    }

    public ArrayList<Integer> getGarage() {
        return garage;
    }

    public int getAfm() {
        return afm;
    }

    public UserTypeEnum getType() {
        return type;
    }

    public static void setCounter(int counter) {
        User.counter = counter;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setGarage(ArrayList<Integer> garage) {
        this.garage = garage;
    }

    public void setAfm(int afm) {
        this.afm = afm;
    }

    public void setType(UserTypeEnum type) {
        this.type = type;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    @Override
    public String toString() {
        String user = "User: " + this.getFirstName() + " " + this.getLastName() + " " + " afm: " + this.getAfm() + " address: " + this.getAddress();
        return user;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User user = (User) o;
        return ((afm == user.afm) && (Objects.equals(Email, user.Email)));
    }

    @Override
    public int hashCode() {
        return Objects.hash(afm, Email);
    }
}
