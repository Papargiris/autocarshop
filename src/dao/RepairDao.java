/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.RepairTypeEnum;
import domain.Repair;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author User
 */
public interface RepairDao {
    public  ArrayList<Repair> getRepairs();
    public void deleteRepairbyId(int id) throws RepairNotFoundException;
    public Repair createRepair(int id,RepairTypeEnum Status,int vehicleId,int cost) throws IdNotAvailableException,VehicleNotFoundException;
    public Repair searchRepairbyId(int id);
    public Set<Integer> searchRepairbyVehicleId(int id) throws VehicleNotFoundException;
    public Set<Integer> searchRepairByDate(Date date);
}
