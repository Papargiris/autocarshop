/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Part;
import domain.PartTypeEnum;
import java.util.ArrayList;
import java.util.Set;

/**
 *
 * @author User
 */
public interface PartDao {

    public Part searchByPartId(int id);

    public ArrayList<Part> getParts();

    public Part createPart(int id, String name, PartTypeEnum type, float cost);

    public Part updatePartId(int partId, int newPartId);

    public void deletePart(int partId);
}
