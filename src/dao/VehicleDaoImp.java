/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.IdNotAvailableException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.User;
import domain.Vehicle;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import services.UserService;

/**
 *
 * @author User
 */
public class VehicleDaoImp implements VehicleDao {

    private static VehicleDaoImp vehcicleDaoInstance;
    private static ArrayList<Vehicle> Vehicles = new ArrayList<Vehicle>();

    private VehicleDaoImp() {

    }

    public static VehicleDaoImp getInstance() {
        if (vehcicleDaoInstance == null) {
            vehcicleDaoInstance = new VehicleDaoImp();
        }
        return vehcicleDaoInstance;
    }

    public ArrayList<Vehicle> getVehicles() {
        return Vehicles;
    }

    @Override
    public Vehicle searchById(int Id) {
        VehicleDaoImp vehicle = VehicleDaoImp.getInstance();
        ArrayList<Vehicle> AllVehicles = vehicle.getVehicles();
        for (Vehicle car : AllVehicles) {
            if (car.getId() == Id) {
                return car;
            }
        }
        return null;
    }

    @Override
    public void updateVehcileId(int id1, int id2) throws IdNotAvailableException {
        ArrayList<Vehicle> Allvehicles = getVehicles();
        for (Vehicle v : Allvehicles) {
            if (v.getId() == id1) {
                v.setId(id2);
            }
        }
    }

    @Override
    public Vehicle createVehicle(int Id, String model, Date CreationDate, String color, int price, int ownerID, String brand) throws UserNotFoundException, IdNotAvailableException {
        ArrayList<Vehicle> vehicles = getVehicles();
        Vehicle car = new Vehicle(Id, model, CreationDate, color, price, ownerID, brand);
        vehicles.add(car);
        return car;
    }

    @Override
    public void deleteVehicleById(int id) throws VehicleNotFoundException {
        ArrayList<Vehicle> vehicles = getVehicles();
        vehicles.removeIf(vehicle -> vehicle.getId() == id);
    }

    public Set<Integer> VehiclesByOwnerId(int OwnerID) throws UserNotFoundException {
        Set<Integer> garage = new TreeSet<Integer>();
        ArrayList<Vehicle> cars = getVehicles();
        for (Vehicle car : cars) {
            if (car.getOwnerID() == OwnerID) {
                garage = new TreeSet<Integer>();
                int carID = car.getId();
                garage.add(carID);
            }
        }
        return garage;
    }

}
