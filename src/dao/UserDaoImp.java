/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.UserNotFoundException;
import domain.UserTypeEnum;
import domain.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Integer;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.RepairService;
import services.UserService;
import services.VehicleService;

/**
 *
 * @author User
 */
public class UserDaoImp implements UserDao {

    private static UserDaoImp userDaoInstance;
    private static ArrayList<User> users = new ArrayList<User>();
    //private static Map<Integer,ArrayList<Integer>> carOwnership;
    private static Map<Integer, Set<Integer>> carOwnership = new HashMap<Integer, Set<Integer>>();

    private UserDaoImp() {

    }

    public static UserDaoImp getInstance() {
        if (userDaoInstance == null) {
            userDaoInstance = new UserDaoImp();
        }

        return userDaoInstance;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public Map<Integer, Set<Integer>> getCarOwnership() {
        return carOwnership;
    }

    public User searchByAfm(int afm) {
        ArrayList<User> Allusers = getUsers();
        for (User user : Allusers) {
            if (user.getAfm() == afm) {
                return user;
            }
        }
        return null;
    }

    public User searchByFirstName(String FirstName) {
        ArrayList<User> Allusers = getUsers();
        for (User user : Allusers) {
            if (user.getEmail().equals(FirstName)) {
                return user;
            }
        }
        return null;
    }

    public User searchByMail(String Mail) {
        ArrayList<User> Allusers = getUsers();
        for (User user : Allusers) {
            if (user.getEmail().equals(Mail)) {
                System.out.println(user.toString());
                return user;
            }
        }
        return null;
    }

    public void updateUserId(int afm, int afm1) {
        ArrayList<User> Allusers = getUsers();
        for (User user : Allusers) {
            if (user.getAfm() == afm) {
                user.setAfm(afm1);
            }
        }
    }

    public HashMap<Integer, Set<Integer>> carOwnershipCreation() throws UserNotFoundException {
        VehicleService service = new VehicleService();
        ArrayList<User> users = getUsers();
        HashMap<Integer, Set<Integer>> relations = new HashMap<Integer, Set<Integer>>();
        for (User user : users) {
            Set<Integer> garage = service.VehiclesByOwnerId(user.getAfm());
            relations = (HashMap<Integer, Set<Integer>>) getCarOwnership();
            relations.put(user.getAfm(), garage);
        }
        return relations;
    }

    public Set<Integer> findVehiclesByUser(int afm) {
        if (this.searchByAfm(afm) != null) {
            HashMap<Integer, Set<Integer>> relations = null;
            try {
                relations = carOwnershipCreation();
            } catch (UserNotFoundException ex) {
                Logger.getLogger(UserDaoImp.class.getName()).log(Level.SEVERE, null, ex);
            }
            Set<Integer> garage = relations.get(afm);
            return garage;
        } else {
            return null;
        }
    }

    public boolean userHasCars(int afm) throws UserNotFoundException {
        HashMap<Integer, Set<Integer>> relations = this.carOwnershipCreation();
        Set<Integer> garage = relations.get(afm);
        if (garage.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void deleteUserById(int afm) {
        ArrayList<User> users = getUsers();
        users.removeIf(user -> user.getAfm() == afm);
    }

    @Override
    public User createUser(int afm, String Email, String FirstName, String LastName, UserTypeEnum type, String Address, String password) {
        ArrayList<User> users = getUsers();
        User user = new User(afm, Email, FirstName, LastName, type, Address, password);
        users.add(user);
        return user;
    }
}
