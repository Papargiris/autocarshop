/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Part;
import domain.PartTypeEnum;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class PartDaoImp implements PartDao {

    private static PartDaoImp partDaoInstance;

    private static ArrayList<Part> parts = new ArrayList<Part>();

    private PartDaoImp() {

    }

    public static PartDaoImp getInstance() {
        if (partDaoInstance == null) {
            partDaoInstance = new PartDaoImp();
        }
        return partDaoInstance;
    }

    public ArrayList<Part> getParts() {
        return parts;
    }

    public Part searchByPartId(int id) {
        ArrayList<Part> Allparts = getParts();
        for (Part p : Allparts) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    
    public Part createPart(int id, String name, PartTypeEnum type, float cost) {
        Part dummie = new Part(id, name, type, cost);
        ArrayList<Part> AllParts = getParts();
        AllParts.add(dummie);
        return dummie;
    }

    @Override
    public Part updatePartId(int partId, int newPartId) {
        ArrayList<Part> AllParts = getParts();
        for (Part part : AllParts) {
            if (part.getId() == partId) {
                part.setId(newPartId);
            }
        }
        return searchByPartId(newPartId);
    }

    @Override
    public void deletePart(int partId) {
        ArrayList<Part> parts = getParts();
        parts.removeIf(part -> part.getId() == partId);
    }

}
