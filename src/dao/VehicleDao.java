/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.IdNotAvailableException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.Vehicle;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author User
 */
public interface VehicleDao {
     
    public ArrayList<Vehicle> getVehicles();
    public Vehicle createVehicle(int Id,String model,Date CreationDate,String color,int price,int ownerID,String brand) throws UserNotFoundException,IdNotAvailableException ;
    public void updateVehcileId(int id1,int id2) throws IdNotAvailableException;
    public void deleteVehicleById(int id) throws VehicleNotFoundException;
    public Set<Integer> VehiclesByOwnerId(int OwnerID)throws UserNotFoundException;
    public Vehicle searchById(int Id);    
}
