/*7
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.RepairTypeEnum;
import domain.Repair;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import services.VehicleService;

/**
 *
 * @author User
 */
public class RepairDaoImp implements RepairDao {

    private static RepairDaoImp repairDaoInstance;
    private static ArrayList<Repair> repairs = new ArrayList<Repair>();

    private RepairDaoImp() {

    }

    public static RepairDaoImp getInstance() {
        if (repairDaoInstance == null) {
            repairDaoInstance = new RepairDaoImp();
        }

        return repairDaoInstance;
    }

    public ArrayList<Repair> getRepairs() {
        return repairs;
    }

    public Set<Integer> searchRepairbyVehicleId(int Vehicleid) throws VehicleNotFoundException {
        VehicleService service = new VehicleService();
        Set<Integer> carRepairs = new TreeSet<Integer>();
        if (service.searchById(Vehicleid) == null) {
            throw new VehicleNotFoundException();
        } else {
            ArrayList<Repair> repairs = getRepairs();
            for (Repair repair : repairs) {
                if (repair.getVehicleId() == Vehicleid) {
                    int rID = repair.getId();
                    carRepairs.add(rID);
                }
            }
        }
        return carRepairs;
    }

    @Override
    public Set<Integer> searchRepairByDate(Date date) {
        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        //opening date of shop
        //String openingDateInString = "07/07/2007";
        //Date openingDate = formatter.parse(openingDateInString);

        //    if(date.after(today) &&(date.before(openingDate))){
        //    }
        Set<Integer> carRepairs = new TreeSet<Integer>();
        ArrayList<Repair> repairs = getRepairs();
        for (Repair repair : repairs) {
            if (repair.getRepairDate() == date) {
                int rID = repair.getId();
                carRepairs.add(rID);
            }
        }
        return carRepairs;
    }

    public Repair searchRepairbyId(int id) {
        ArrayList<Repair> Allrepairs = getRepairs();
        for (Repair r : Allrepairs) {
            if (r.getId() == id) {
                return r;
            }
        }
        return null;
    }

    public void deleteRepairbyId(int id) throws RepairNotFoundException {
        if (searchRepairbyId(id) != null) {
            ArrayList<Repair> repairs = getRepairs();
            repairs.removeIf(repair -> repair.getId() == id);
        } else {
            throw new RepairNotFoundException();
        }
    }

    public Repair createRepair(int id, RepairTypeEnum Status, int vehicleId, int cost) throws IdNotAvailableException, VehicleNotFoundException {
        VehicleService v = new VehicleService();
        if (this.searchRepairbyId(id) != null) {
            throw new IdNotAvailableException();
        }
        if (v.searchById(vehicleId) != null) {
            Date repairDate = new Date();
            Repair dummie = new Repair(id, Status, vehicleId, cost, repairDate);
            ArrayList<Repair> AllRepairs = getRepairs();
            AllRepairs.add(dummie);
            return dummie;
        } else {
            throw new VehicleNotFoundException();
        }
    }
}
