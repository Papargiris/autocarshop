/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Exceptions.UserNotFoundException;
import domain.UserTypeEnum;
import domain.User;
import java.util.Set;

/**
 *
 * @author User
 */
public interface UserDao {
      public User createUser(int afm,String Email,String FirstName,String LastName,UserTypeEnum type,String Address,String password);
      public User searchByAfm(int afm);
      public User searchByFirstName(String FirstName);
      public User searchByMail(String Mail);
      public void updateUserId(int afm,int afm1);
      public Set<Integer> findVehiclesByUser(int afm);
      public void deleteUserById(int afm);
}
