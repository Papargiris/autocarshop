/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.Repair;
import domain.RepairTypeEnum;
import domain.User;
import domain.Vehicle;
import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.RepairService;

/**
 *
 * @author User
 */
public class RepairController extends TypeController implements Controller {

    RepairService repairService = new RepairService();

    private static final String REPAIR_MENU_TITLE = "REPAIR MENU";

    private static final HashMap<Integer, String> REPAIR_MENU = new HashMap<Integer, String>() {
        {
            put(0, "******___GO TO CENTRAL MENU******");
            put(1, "******___CREATE REPAIR******");
            put(2, "******___FIND REPAIR BY ID******");
            put(3, "******___DELETE REPAIR BY ID******");
            put(4, "******___FIND ALL REPAIRS__******");
            put(5, "******___GET TOTAL REPAIR COST BY REPAIR ID__******");
        }
    ;

    };

    public static String getREPAIR_MENU_TITLE() {
        return REPAIR_MENU_TITLE;
    }

    public static Map<Integer, String> getREPAIR_MENU() {
        return REPAIR_MENU;
    }

    public Integer validScannedRepairId() {
        String idInput;
        int checkerId = 0;
        int idLength = 0;
        System.out.println("Enter id number:");
        idInput = ScanHelper.getScanner().nextLine();
        idInput.trim();
        boolean safety = this.isIdInteger(idInput);
        if (safety == true) {
            idLength = idInput.length();
            checkerId = Integer.parseInt(idInput);
        }
        while (idLength != 4 || checkerId < 0 || safety == false) {
            System.out.println("Enter id number,it must contains exactly 4 digits:");
            idInput = ScanHelper.getScanner().nextLine();
            idInput.trim();
            safety = isIdInteger(idInput);
            if (safety == true) {
                idLength = idInput.length();
                checkerId = Integer.parseInt(idInput);
            }
        }

        Integer validInteger = Integer.parseInt(idInput);
        return validInteger;
    }

    public void findRepairById() {
        int id = this.validScannedRepairId();
        try {
            Repair foundRepair = repairService.searchRepairbyId(id);
            System.out.println(foundRepair.toString());
        } catch (RepairNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void deleteRepairById() {
        ArrayList<Repair> repairs = repairService.findAllRepairs();
        if (repairs.isEmpty()) {
            System.out.println("There are no repairs registred!");
        } else {
            for (Repair repair : repairs) {
                System.out.println(repair.toString());
            }
            System.out.println("Enter the id number of the repair you want to delete below");
            int id = this.validScannedRepairId();

            try {
                repairService.deleteRepairbyId(id);
            } catch (IdNotAvailableException ex) {
                System.out.println(ex.toString());
            } catch (RepairNotFoundException ex) {
                System.out.println(ex.toString());
            }
            System.out.println("Repair with id:" + id + " was deleted!");
        }
    }

    public Float validScannedRepairCost() {
        String costInput;
        int costInputLenght = 0;
        System.out.println("Enter a price number:");
        costInput = ScanHelper.getScanner().nextLine();
        costInput.trim();
        boolean safety = this.isFloat(costInput);
        if (safety == true) {
            costInputLenght = Integer.parseInt(costInput);
        }
        while (costInputLenght < 0 || safety == false) {
            System.out.println("Enter repair price,it must be at list a 3 digits number:");
            costInput = ScanHelper.getScanner().nextLine();
            costInput.trim();
            safety = isIdInteger(costInput);
            if (safety == true) {
                costInputLenght = Integer.parseInt(costInput);
            }
        }
        Float validFloat = Float.parseFloat(costInput);
        return validFloat;
    }

    public RepairTypeEnum validScannedRepairStatus() {
        String repairStatusInput;
        int checkerRepairStatus = 0;
        System.out.println("Enter 1 if you want the repair to have pending status or 2 if you want to have done status:");
        repairStatusInput = ScanHelper.getScanner().nextLine();
        repairStatusInput.trim();
        boolean safety = this.isIdInteger(repairStatusInput);
        if (safety == true) {
            checkerRepairStatus = Integer.parseInt(repairStatusInput);
        }
        while (checkerRepairStatus != 1 || checkerRepairStatus != 2 || safety == false) {
            System.out.println("Enter 1 if you want the repair to have pending status or 2 if you want to have done status");
            repairStatusInput = ScanHelper.getScanner().nextLine();
            repairStatusInput.trim();
            safety = isIdInteger(repairStatusInput);
            if (safety == true) {
                checkerRepairStatus = Integer.parseInt(repairStatusInput);
            }
        }

        Integer validInteger = Integer.parseInt(repairStatusInput);
        if (validInteger == 1) {
            return RepairTypeEnum.PENDING;
        } else {
            return RepairTypeEnum.DONE;
        }
    }

    public void createRepair() {
        int id = this.validScannedRepairId();
        int vehicleId = this.validScannedRepairId();
        float cost = this.validScannedRepairCost();
        RepairTypeEnum repairTypeEnum = this.validScannedRepairStatus();
        try {
            repairService.createRepair(id, repairTypeEnum, vehicleId, id);
        } catch (RepairNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IdNotAvailableException ex) {
            System.out.println(ex.getMessage());
        } catch (VehicleNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void getAllRepairs() {
        ArrayList<Repair> repairs = repairService.findAllRepairs();
        if (repairs.isEmpty()) {
            System.out.println("There are no repairs registered!");
        } else {
            for (Repair repair : repairs) {
                System.out.println(repair.toString());
            }
        }
    }

    public void getTotalRepairCostByRepairId() {
        int id = this.validScannedRepairId();
        if (repairService.findAllRepairs().isEmpty()) {
            System.out.println("There are no repairs!Better put some money in marketing!");
        }
        try {
            repairService.totalRepairCost(id);
        } catch (RepairNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public void selector() throws Exception {
        int selection = this.printMenu(REPAIR_MENU, REPAIR_MENU_TITLE);
        switch (selection) {
            case 1:
                createRepair();
                selector();
                break;
            case 2:
                this.findRepairById();
                selector();
                break;
            case 3:
                deleteRepairById();
                selector();
                break;
            case 4:
                getAllRepairs();
                selector();
                break;
            case 5:
                getTotalRepairCostByRepairId();
                selector();
                break;
            case 0:
                CentralController centralMenu = new CentralController();
                centralMenu.printMenu(CentralController.getCENTRAL_MENU(), CentralController.getCENTRAL_MENU_TITLE());
                break;
        }
    }
}
