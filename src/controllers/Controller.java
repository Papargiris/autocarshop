/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author User
 */
public interface Controller {

    default public int printMenu(Map<Integer, String> data, String menuTitle) {
        System.out.println("------------------------" + menuTitle + "-----------------------------");
        int selection = -1;
        do {
            for (Integer key : data.keySet()) {
                System.out.println(key + ")" + " " + data.get(key));
            }
            try {
                selection = this.validScannedInteger();
            } catch (NumberFormatException e1) {
                System.out.println("Enter a valid option,valid options range from 0 to " + data.size());
                while (selection == -1) {
                    try {
                        selection = this.validScannedInteger();
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a valid option,valid options range from 0 to " + data.size());
                    }
                }
                if (validIntegerSelection(selection, data.size()) == false) {
                    System.out.println("Enter a valid option,valid options range from 0 to " + data.size());
                }
            }
        } while (validIntegerSelection(selection, data.size()) == false);
        return selection;
    }

    public void selector() throws Exception;

    default public boolean validIntegerSelection(int selection, int maxSelections) {
        ArrayList<Integer> options = new ArrayList<Integer>();
        for (int i = 0; i < maxSelections; i++) {
            options.add(i);
        }
        boolean valid = options.contains(selection);
        return valid;
    }

    default public Integer validScannedInteger() throws NumberFormatException {
        String wrongInput;
        System.out.println("Enter number:");
        wrongInput = ScanHelper.getScanner().nextLine();
        wrongInput.trim();
        boolean safety = checksScaForNumber(wrongInput);
        if ((safety == true) && (wrongInput.length() > 10)) {
            throw new NumberFormatException();
        }
        while (safety == false) {
            System.out.println("You must enter a number!Only numbers are acceptable input");
            wrongInput = ScanHelper.getScanner().nextLine();
            safety = checksScaForNumber(wrongInput);
        }
        Integer validInteger = Integer.parseInt(wrongInput);
        return validInteger;
    }

    default public boolean checksScaForNumber(String input) {
        boolean isNumber = input.chars().allMatch(Character::isDigit);
        return isNumber;
    }

    default public boolean isIdInteger(String idInput) {
        if (checksScaForNumber(idInput) == true) {
            int checkId = idInput.length();
            if (checkId > 9) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    default public boolean isFloat(String input) {
        try {
            double d = Double.parseDouble(input);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    default public boolean notBlankString(String checker) {
        boolean notBlanck = (checker.matches(".*\\w.*") && checker.length() > 0);
        return notBlanck;
    }

}
