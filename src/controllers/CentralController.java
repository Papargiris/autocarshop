/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.UIManager.put;

/**
 *
 * @author User
 */
public class CentralController extends TypeController implements Controller {

    UserController userMenu = new UserController();
    RepairController repairMenu = new RepairController();
    PartController partMenu = new PartController();
    VehicleController vehicleMenu = new VehicleController();
    TypeController exitMenu = new TypeController();

    private static final HashMap<Integer, String> CENTRAL_MENU = new HashMap<Integer, String>() {
        {
            put(0, "******___TERMINATE PROGRAM__******");
            put(1, "******___USER MENU__******");
            put(2, "******___VEHICLE MENU__******");
            put(3, "******___REPAIR MENU__******");
            put(4, "******___PART MENU__******");

        }
    ;

    };


    public static HashMap<Integer, String> getCENTRAL_MENU() {
        return CENTRAL_MENU;
    }
    private static final String CENTRAL_MENU_TITLE = "CENTRAL MENU";

    public static String getCENTRAL_MENU_TITLE() {
        return CENTRAL_MENU_TITLE;
    }

    @Override
    public void selector() throws Exception {
        int selection = this.printMenu(CENTRAL_MENU, CENTRAL_MENU_TITLE);
        switch (selection) {
            case 1: {
                try {
                    userMenu.selector();
                } catch (Exception ex) {
                    System.out.println("Something went wrong!");
                    selector();
                }
            }
            break;
            case 2:
        {
            try {
                vehicleMenu.selector();
            } catch (Exception ex) {
                System.out.println("Something went wrong");
                selector();
            }
        }
                break;
            case 3:
                repairMenu.selector();
                break;
            case 4:
                partMenu.selector();
                break;
            case 0:
                System.out.println("END OF PROGRAM");
                exitMenu.closeScanner();
                break;
        }
    }

}
