/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.IdNotAvailableException;
import Exceptions.PartNotFoundException;
import Exceptions.RepairNotFoundException;
import domain.Part;
import domain.PartTypeEnum;
import domain.Repair;
import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.PartService;

/**
 *
 * @author User
 */
public class PartController extends TypeController implements Controller {

    PartService partService = new PartService();
    private static final String PART_MENU_TITLE = "USER MENU";

    private static final HashMap<Integer, String> PART_MENU = new HashMap<Integer, String>() {
        {
            put(0, "******___GO TO CENTRAL MENU******");
            put(1, "******___CREATE PART******");
            put(2, "******___FIND PART BY ID******");
            put(3, "******___UPDATE PART ID BY ID******");
            put(4, "******___DELETE PART BY ID******");
            put(5, "******___FIND ALL PARTS__******");
        }
    ;

    };

    public static String getREPAIR_MENU_TITLE() {
        return PART_MENU_TITLE;
    }

    public static Map<Integer, String> getREPAIR_MENU() {
        return PART_MENU;
    }

    public Integer validScannedPartId() {
        String idInput;
        int checkerId = 0;
        int idLength = 0;
        System.out.println("Enter id number:");
        idInput = ScanHelper.getScanner().nextLine();
        idInput.trim();
        boolean safety = this.isIdInteger(idInput);
        if (safety == true) {
            idLength = idInput.length();
            checkerId = Integer.parseInt(idInput);
        }
        while (idLength != 4 || checkerId < 0 || safety == false) {
            System.out.println("Enter id number,it must contains exactly 4 digits:");
            idInput = ScanHelper.getScanner().nextLine();
            idInput.trim();
            safety = isIdInteger(idInput);
            if (safety == true) {
                idLength = idInput.length();
                checkerId = Integer.parseInt(idInput);
            }
        }
        Integer validInteger = Integer.parseInt(idInput);
        return validInteger;
    }

    public void getAllParts() {
        ArrayList<Part> parts = partService.findAllParts();
        if (parts.isEmpty()) {
            System.out.println("There are no parts registered!");
        } else {
            for (Part part : parts) {
                System.out.println(part.toString());
            }
        }
    }

    public PartTypeEnum validScannedPartStatus() {
        String statusInput;
        int checkerStatusInput = 0;
        int idLength = 0;
        System.out.println("Enter 1 if you want the repair to have used status,2 if you want to have immitation status or 3 if you want your part status to be MAMA");
        statusInput = ScanHelper.getScanner().nextLine();
        statusInput.trim();
        boolean safety = this.isIdInteger(statusInput);
        if (safety == true) {
            checkerStatusInput = Integer.parseInt(statusInput);
        }
        while (checkerStatusInput != 1 || checkerStatusInput != 2 || checkerStatusInput != 3 || safety == false) {
            System.out.println("Enter 1 if you want the repair to have used status ,2 if you want to have immitation status or 3 if you want part status to be MAMA");
            statusInput = ScanHelper.getScanner().nextLine();
            statusInput.trim();
            safety = isIdInteger(statusInput);
            if (safety == true) {
                checkerStatusInput = Integer.parseInt(statusInput);
            }
        }

        Integer validInteger = Integer.parseInt(statusInput);
        if (validInteger == 1) {
            return PartTypeEnum.IMITATION;
        } else if (validInteger == 2) {
            return PartTypeEnum.USED;
        } else {
            return PartTypeEnum.MAMA;
        }
    }

    public String validScannedPartNameInput() {
        String stringInput;
        System.out.println("Enter your car's model:");
        stringInput = ScanHelper.getScanner().nextLine();
        stringInput.trim();
        boolean atleastOneAlpha = stringInput.matches(".*[a-zA-Z]+.*");
        boolean notBlanck = this.notBlankString(stringInput);
        while (stringInput.isEmpty() || atleastOneAlpha == false || notBlanck == false) {
            System.out.println("Part name can't be empty and must have a name for your part!Please enter a name for your part again:");
            stringInput = ScanHelper.getScanner().nextLine();
        }
        return stringInput;
    }

    public Float validScannedPartCost() {
        String costInput;
        int costInputLenght = 0;
        System.out.println("Enter a price number for the part:");
        costInput = ScanHelper.getScanner().nextLine();
        costInput.trim();
        boolean safety = this.isFloat(costInput);
        if (safety == true) {
            costInputLenght = Integer.parseInt(costInput);
        }
        while (costInputLenght <= 0 || safety == false) {
            System.out.println("Enter part price,price must be greater than zero.Nothting in life is free ma nigga!:");
            costInput = ScanHelper.getScanner().nextLine();
            costInput.trim();
            safety = isIdInteger(costInput);
            if (safety == true) {
                costInputLenght = Integer.parseInt(costInput);
            }
        }
        Float validFloat = Float.parseFloat(costInput);
        return validFloat;
    }

    public void createPart() {
        int id = this.validScannedPartId();
        String name = this.validScannedPartNameInput();
        PartTypeEnum status = this.validScannedPartStatus();
        float cost = this.validScannedPartCost();
        try {
            Part part = partService.createPart(id, name, status, cost);
            System.out.println(part.toString() + " was created!");
        } catch (IdNotAvailableException ex) {
            System.out.println(ex.getMessage());
        } catch (PartNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void updatePartById() {
        ArrayList<Part> parts = partService.findAllParts();
        if (parts.isEmpty()) {
            System.out.println("There are no parts registred!");
        } else {
            for (Part part : parts) {
                System.out.println(part.toString());
            }
            System.out.println("Enter the id number of the part you want to update");
            int id = this.validScannedPartId();
            System.out.println("Give new id number to the part");
            int newId = this.validScannedPartId();
            try {
                try {
                    Part updatedPart = partService.updatePartId(id, newId);
                } catch (PartNotFoundException ex) {
                    System.out.println(ex.toString());
                }
                System.out.println("Part  id was updated his from " + id + " to " + newId);
            } catch (IdNotAvailableException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void deletePartById() {
        ArrayList<Part> parts = partService.findAllParts();
        if (parts.isEmpty()) {
            System.out.println("There are no repairs registred!");
        } else {
            for (Part part : parts) {
                System.out.println(part.toString());
            }
            System.out.println("Enter the id number of the part you want to delete below");
            int id = this.validScannedPartId();
            try {
                partService.deletePart(id);
                System.out.println("Part with id:" + id + " was deleted!");
            } catch (PartNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void findPartById() {
        int id = this.validScannedPartId();
        try {
            Part foundPart = partService.searchByPartId(id);
            System.out.println(foundPart.toString());
        } catch (PartNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void selector() {
        int selection = this.printMenu(PART_MENU, PART_MENU_TITLE);
        switch (selection) {
            case 1:
                createPart();
                selector();
                break;
            case 2:
                findPartById();
                selector();
                break;
            case 3:
                updatePartById();
                selector();
                break;
            case 4:
                deletePartById();
                selector();
                break;
            case 5:
                getAllParts();
                selector();
                break;
            case 0:
                CentralController centralMenu = new CentralController();
                centralMenu.printMenu(CentralController.getCENTRAL_MENU(), CentralController.getCENTRAL_MENU_TITLE());
                break;
        }
    }

}
