/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.Repair;
import domain.User;
import domain.Vehicle;
import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.PartService;
import services.RepairService;
import services.UserService;
import services.VehicleService;

/**
 *
 * @author User
 */
public class VehicleController extends TypeController implements Controller {

    UserService userService = new UserService();
    VehicleService vehicleService = new VehicleService();
    PartService partService = new PartService();
    RepairService repairService = new RepairService();
    UserController userController = new UserController();

    private static final String VEHICLE_MENU_TITLE = "USER MENU";

    private static final HashMap<Integer, String> VEHICLE_MENU = new HashMap<Integer, String>() {
        {
            put(0, "******___GO TO CENTRAL MENU******");
            put(1, "******___CREATE VEHICLE******");
            put(2, "******___FIND VEHICLE BY ID******");
            put(3, "******___UPDATE VEHICLE BY ID******");
            put(4, "******___DELETE VEHICLE BY ID******");
            put(5, "******___FIND ALL VEHICLES__******");
            put(6, "******___FIND ALL  REPAIRSS FOR A VEHICLE BY VEHICLE ID__******");
        }
    ;

    };

    public static String getREPAIR_MENU_TITLE() {
        return VEHICLE_MENU_TITLE;
    }

    public static Map<Integer, String> getREPAIR_MENU() {
        return VEHICLE_MENU;
    }

    public Integer validScannedVehicleId() {
        String idInput;
        int checkerId = 0;
        int idLength = 0;
        System.out.println("Enter id number:");
        idInput = ScanHelper.getScanner().nextLine();
        idInput.trim();
        boolean safety = this.isIdInteger(idInput);
        if (safety == true) {
            idLength = idInput.length();
            checkerId = Integer.parseInt(idInput);
        }
        while (idLength != 4 || checkerId < 0 || safety == false) {
            System.out.println("Enter id number,it must contains exactly 4 digits:");
            idInput = ScanHelper.getScanner().nextLine();
            idInput.trim();
            safety = isIdInteger(idInput);
            if (safety == true) {
                idLength = idInput.length();
                checkerId = Integer.parseInt(idInput);
            }
        }

        Integer validInteger = Integer.parseInt(idInput);
        return validInteger;
    }

    public Integer validScannedOwnerId() {
        String ownerIdInput;
        int checkerId = 0;
        int idLength = 0;
        userController.getAllUsers();
        System.out.println("Enter  owner's afm number:");
        ownerIdInput = ScanHelper.getScanner().nextLine();
        ownerIdInput.trim();
        boolean safety = this.isIdInteger(ownerIdInput);
        if (safety == true) {
            idLength = ownerIdInput.length();
            checkerId = Integer.parseInt(ownerIdInput);
        }
        while (idLength != 9 || checkerId < 0 || safety == false) {
            userController.getAllUsers();
            System.out.println("Enter owner's afm,it must contains exactly 9 digits:");
            ownerIdInput = ScanHelper.getScanner().nextLine();
            ownerIdInput.trim();
            safety = isIdInteger(ownerIdInput);
            if (safety == true) {
                idLength = ownerIdInput.length();
                checkerId = Integer.parseInt(ownerIdInput);
            }
        }

        Integer validInteger = Integer.parseInt(ownerIdInput);
        return validInteger;
    }

    public Integer validScannedPrice() {
        String priceInput;
        int priceInputPositive = 0;
        int priceInputLength = 0;
        System.out.println("Enter a price number:");
        priceInput = ScanHelper.getScanner().nextLine();
        priceInput.trim();
        boolean safety = this.isIdInteger(priceInput);
        if (safety == true) {
            priceInputLength = priceInput.length();
            priceInputPositive = Integer.parseInt(priceInput);
        }
        while (priceInputLength < 3 || priceInputPositive < 0 || safety == false) {
            System.out.println("Enter vehicle price,it must be at list a 3 digits number:");
            priceInput = ScanHelper.getScanner().nextLine();
            priceInput.trim();
            safety = isIdInteger(priceInput);
            if (safety == true) {
                priceInputLength = priceInput.length();
                priceInputPositive = Integer.parseInt(priceInput);
            }
        }

        Integer validInteger = Integer.parseInt(priceInput);
        return validInteger;
    }

    public String validScannedBrandInput() {
        String stringInput;
        System.out.println("Enter your car's brand:");
        stringInput = ScanHelper.getScanner().nextLine();
        stringInput.trim();
        boolean atleastOneAlpha = stringInput.matches(".*[a-zA-Z]+.*");
        boolean notBlanck = this.notBlankString(stringInput);
        while (stringInput.isEmpty() || atleastOneAlpha == false || notBlanck == false) {
            System.out.println("Brand can't be empty and must have a brand!Please enter your brand again:");
            stringInput = ScanHelper.getScanner().nextLine();
        }
        return stringInput;
    }

    public String validScannedModelInput() {
        String stringInput;
        System.out.println("Enter your car's model:");
        stringInput = ScanHelper.getScanner().nextLine();
        stringInput.trim();
        boolean atleastOneAlpha = stringInput.matches(".*[a-zA-Z]+.*");
        boolean notBlanck = this.notBlankString(stringInput);
        while (stringInput.isEmpty() || atleastOneAlpha == false || notBlanck == false) {
            System.out.println("Model can't be empty and must have a name for your model!Please enter a name for your model again:");
            stringInput = ScanHelper.getScanner().nextLine();
        }
        return stringInput;
    }

    public String validScannedColorInput() {
        String stringInput;
        System.out.println("Enter your car's color:");
        stringInput = ScanHelper.getScanner().nextLine();
        stringInput.trim();
        boolean atleastOneAlpha = stringInput.matches(".*[a-zA-Z]+.*");
        boolean notBlanck = this.notBlankString(stringInput);
        while (stringInput.isEmpty() || atleastOneAlpha == false || notBlanck == false) {
            System.out.println("Color can't be empty and must have a color for your car!Please enter a color for your car again:");
            stringInput = ScanHelper.getScanner().nextLine();
        }
        return stringInput;
    }

    public void createVehicle() throws Exception {
        int id = this.validScannedVehicleId();
        int ownerId = this.validScannedOwnerId();
        int price = this.validScannedPrice();
        String model = this.validScannedModelInput();
        String brand = this.validScannedBrandInput();
        String color = this.validScannedColorInput();
        Date creationDate = new Date();
        try {
            vehicleService.createVehicle(id, model, creationDate, color, price, ownerId, brand);
        } catch (UserNotFoundException ex) {
            if (userService.findAllUsers().isEmpty() == true) {
                System.out.println("There are no users registred in the system.You must register the owner as a user first!");
                userController.selector();
            } else {
                System.out.println(ex.getMessage());
            }

        } catch (IdNotAvailableException ex) {
            System.out.println(ex.toString());
        }
    }

    public void findVehiclebyId() {
        int id = this.validScannedVehicleId();
        try {
            User foundVehicle = userService.searchByAfm(id);
            System.out.println(foundVehicle.toString());
        } catch (UserNotFoundException ex) {
            System.out.println(ex.getMessage());

        }
    }

    public void updateVehiclebyId() {
        ArrayList<Vehicle> vehicles = vehicleService.findAllVehicle();
        if (vehicleService.findAllVehicle().isEmpty()) {
            System.out.println("There are no Vehicles registred!");
        } else {
            for (Vehicle vehicle : vehicles) {
                System.out.println(vehicle.toString());
            }
            System.out.println("Enter the afm number of the user you want to update");
            int id = this.validScannedVehicleId();
            System.out.println("Give new afm to the user");
            int newId = this.validScannedVehicleId();
            try {
                Vehicle updatedVehicle = vehicleService.updateVehcileId(id, newId);
                System.out.println("Vehicle " + updatedVehicle.getBrand() + " " + updatedVehicle.getModel() + " updated his from " + id + " to " + newId);
            } catch (IdNotAvailableException ex) {
                System.out.println(ex.toString());
            } catch (VehicleNotFoundException ex) {
                System.out.println(ex.toString());
            }
        }
    }

    public void deleteVehiclebyId() {
        ArrayList<Vehicle> vehicles = vehicleService.findAllVehicle();
        if (vehicleService.findAllVehicle().isEmpty()) {
            System.out.println("There are no vehicles registred!");
        } else {
            for (Vehicle vehicle : vehicles) {
                System.out.println(vehicle.toString());
            }
            System.out.println("Enter the id number of the vehicle you want to delete below");
            int id = this.validScannedVehicleId();
            try {
                vehicleService.deleteVehicleById(id);
                System.out.println("Vehicle with id:" + id + " was deleted!");
            } catch (VehicleNotFoundException ex) {
                System.out.println(ex.getMessage());
            } catch (RepairNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void getAllRepairsByVehicleId() {
        ArrayList<Vehicle> vehicles = vehicleService.findAllVehicle();
        if (vehicles.isEmpty()) {
            System.out.println("There are no vehicles registered!");
        } else {
            for (Vehicle vehicle : vehicles) {
                System.out.println(vehicle.toString());
            }
            int vehicleId = validScannedVehicleId();
            Set<Integer> repairs = new TreeSet<Integer>();
            try {
                Vehicle vehicle = vehicleService.searchById(vehicleId);
                System.out.println(vehicle.toString());
                repairs = repairService.searchRepairbyVehicleId(vehicleId);
                if (repairs.isEmpty()) {
                    System.out.println(vehicle.toString() + " has no repairs");
                } else {
                    for (Integer repairId : repairs) {
                        System.out.println("Repairs of " + vehicle.toString());
                        try {
                            Repair repair = repairService.searchRepairbyId(repairId);
                            System.out.println(repair.toString());
                        } catch (RepairNotFoundException ex) {
                            System.out.println(ex.toString());
                        }
                    }
                }
            } catch (VehicleNotFoundException ex) {
                System.out.println(ex.getMessage());
            }

        }
    }

    public void getAllVehicles() {
        ArrayList<Vehicle> vehicles = vehicleService.findAllVehicle();
        if (vehicles.isEmpty()) {
            System.out.println("There are no vehicles registered!");
        } else {
            for (Vehicle vehicle : vehicles) {
                System.out.println(vehicle.toString());
            }
        }
    }

    @Override
    public void selector() throws Exception {
        int selection = this.printMenu(VEHICLE_MENU, VEHICLE_MENU_TITLE);
        switch (selection) {
            case 1:
                createVehicle();
                selector();
                break;
            case 2:
                findVehiclebyId();
                selector();
                break;
            case 3:
                updateVehiclebyId();
                selector();
                break;
            case 4:
                deleteVehiclebyId();
                selector();
                break;
            case 5:
                getAllVehicles();
                selector();
                break;
            case 6:
                getAllRepairsByVehicleId();
                selector();
                break;
            case 0:
                CentralController centralMenu = new CentralController();
                centralMenu.printMenu(CentralController.getCENTRAL_MENU(), CentralController.getCENTRAL_MENU_TITLE());
                break;
        }
    }

}
