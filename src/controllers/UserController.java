/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.EmailNotAvailableException;
import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import dao.UserDaoImp;
import domain.Repair;
import domain.User;
import domain.UserTypeEnum;
import domain.Vehicle;
import helpers.ScanHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.PartService;
import services.RepairService;
import services.UserService;
import services.VehicleService;

/**
 *
 * @author User
 */
public class UserController extends TypeController implements Controller {

    UserService userService = new UserService();
    VehicleService vehicleService = new VehicleService();
    PartService partService = new PartService();
    RepairService repairService = new RepairService();

    private static final String USER_MENU_TITLE = "USER MENU";

    private static final HashMap<Integer, String> USER_MENU = new HashMap<Integer, String>() {
        {
            put(0, "******___GO TO CENTRAL MENU******");
            put(1, "******___CREATE USER******");
            put(2, "******___FIND USER BY ID******");
            put(3, "******___UPDATE USER BY ID******");
            put(4, "******___DELETE USER BY ID******");
            put(5, "******___FIND ALL USERS__******");
            put(6, "******___FIND ALL CARS BY USER ID__******");
            put(7, "******___FIND ALL CAR REPAIRS BY USER ID__******");

        }
    ;

    };

    public static String getUSER_MENU_TITLE() {
        return USER_MENU_TITLE;
    }

    public static Map<Integer, String> getUSER_MENU() {
        return USER_MENU;
    }

    public boolean checksScannedForName(String input) {
        boolean isNumber = input.chars().allMatch(Character::isLetter);
        return isNumber;
    }

    public boolean isValidEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public String validScannedEmail() {
        String emailInput;
        System.out.println("Enter your email:");
        emailInput = ScanHelper.getScanner().nextLine();
        emailInput.trim();
        while (emailInput.isEmpty()) {
            System.out.println("You must have an email enter your email please:");
            emailInput = ScanHelper.getScanner().nextLine();
        }
        boolean safety = isValidEmail(emailInput);
        while (safety == false) {
            System.out.println("Email was mot valid,please enter your email again:");
            emailInput = ScanHelper.getScanner().nextLine();
            emailInput.trim();
            while (emailInput.isEmpty()) {
                System.out.println("You must have an email,please enter your email again please:");
                emailInput = ScanHelper.getScanner().nextLine();
            }
            safety = isValidEmail(emailInput);
        }
        return emailInput;
    }

    public String validScannedPassword() {
        String passwordInput;
        System.out.println("Enter your password:");
        passwordInput = ScanHelper.getScanner().nextLine();
        passwordInput.trim();
        while (passwordInput.isEmpty() || passwordInput.length() < 4) {
            System.out.println("Password can't be empty and must have at least 4 digits!Please enter your password again:");
            passwordInput = ScanHelper.getScanner().nextLine();
        }
        return passwordInput;
    }

    public String validScannedAddress() {
        String addressInput;
        System.out.println("Enter your address:");
        addressInput = ScanHelper.getScanner().nextLine();
        addressInput.trim();
        boolean atleastOneAlpha = addressInput.matches(".*[a-zA-Z]+.*");
        while (addressInput.isEmpty() || atleastOneAlpha == false) {
            System.out.println("Address can't be empty and must have an address!Please enter your address again:");
            addressInput = ScanHelper.getScanner().nextLine();
        }
        return addressInput;
    }

    public String validScannedLastName() {
        String firstNameInput;
        System.out.println("Enter your lastname:");
        firstNameInput = ScanHelper.getScanner().nextLine();
        firstNameInput.trim();
        while (firstNameInput.isEmpty()) {
            System.out.println("You must have a last name your not Madona or Sting,enter your lastname again please:");
            firstNameInput = ScanHelper.getScanner().nextLine();
        }
        boolean safety = checksScannedForName(firstNameInput);
        while (safety == false) {
            System.out.println("Something went wrong please enter your lastname again:");
            firstNameInput = ScanHelper.getScanner().nextLine();
            firstNameInput.trim();
            while (firstNameInput.isEmpty()) {
                System.out.println("You must have a last name your not Madona or Sting,enter your lastname again please::");
                firstNameInput = ScanHelper.getScanner().nextLine();
            }
            safety = checksScannedForName(firstNameInput);
        }
        return firstNameInput;
    }

    public String validScannedName() {
        String firstNameInput;
        System.out.println("Enter your name:");
        firstNameInput = ScanHelper.getScanner().nextLine();
        firstNameInput.trim();
        while (firstNameInput.isEmpty()) {
            System.out.println("You must have a name,enter your name again please:");
            firstNameInput = ScanHelper.getScanner().nextLine();
        }
        boolean safety = checksScannedForName(firstNameInput);
        while (safety == false) {
            System.out.println("Something went wrong please enter your name again:");
            firstNameInput = ScanHelper.getScanner().nextLine();
            firstNameInput.trim();
            while (firstNameInput.isEmpty()) {
                System.out.println("You must have a name,enter your lastname again please::");
                firstNameInput = ScanHelper.getScanner().nextLine();
            }
            safety = checksScannedForName(firstNameInput);
        }
        return firstNameInput;
    }

    public Integer validScannedAfm() {
        String afmInput;
        int checkerAfm = 0;
        int afmLength = 0;
        System.out.println("Enter afm number:");
        afmInput = ScanHelper.getScanner().nextLine();
        afmInput.trim();
        boolean safety = this.isIdInteger(afmInput);
        if (safety == true) {
            afmLength = afmInput.length();
            checkerAfm = Integer.parseInt(afmInput);
        }
        while (afmLength != 9 || checkerAfm < 0 || safety == false) {
            System.out.println("Enter afm number,it must contains exactly 9 digits:");
            afmInput = ScanHelper.getScanner().nextLine();
            afmInput.trim();
            safety = isIdInteger(afmInput);
            if (safety == true) {
                afmLength = afmInput.length();
                checkerAfm = Integer.parseInt(afmInput);
            }
        }

        Integer validInteger = Integer.parseInt(afmInput);
        return validInteger;
    }

    public void createUser() throws UserNotFoundException, IdNotAvailableException, EmailNotAvailableException {
        int afm = this.validScannedAfm();
        String name = this.validScannedName();
        String lastName = this.validScannedLastName();
        String email = this.validScannedEmail();
        String password = this.validScannedPassword();
        String address = this.validScannedAddress();
        try {
            userService.createUser(afm, email, name, lastName, UserTypeEnum.USER, address, password);
        } catch (IdNotAvailableException ex) {
            System.out.println(ex.getMessage());
            User checker = userService.searchByAfm(afm);
            while (checker != null) {
                afm = this.validScannedAfm();
                checker = userService.searchByAfm(afm);
            }
            userService.createUser(afm, email, name, lastName, UserTypeEnum.USER, address, password);
        } catch (EmailNotAvailableException ex) {
            System.out.println(ex.getMessage());
            User checker = userService.searchByMail(email);
            while (checker != null) {
                email = this.validScannedEmail();
                checker = userService.searchByMail(email);
            }
            userService.createUser(afm, email, name, lastName, UserTypeEnum.USER, address, password);
        }
        User created = userService.searchByAfm(afm);
        System.out.println("User : " + created.toString() + " registered successfully");
    }

    public void findUseById() {
        int afm = this.validScannedAfm();
        try {
            User foundUser = userService.searchByAfm(afm);
            System.out.println(foundUser.toString());
        } catch (UserNotFoundException ex) {
            System.out.println(ex.getMessage());

        }
    }

    public void updateUserById() {
        ArrayList<User> users = userService.findAllUsers();
        if (userService.findAllUsers().isEmpty()) {
            System.out.println("There are mo users registred!");
        } else {
            for (User user : users) {
                System.out.println(user.toString());
            }
            System.out.println("Enter the afm number of the user you want to update");
            int afm = this.validScannedAfm();
            System.out.println("Give new afm to the user");
            int newAfm = this.validScannedAfm();
            try {
                User updatedUser = userService.updateUserId(afm, newAfm);
                System.out.println("User " + updatedUser.getLastName() + " updated his from " + afm + " to " + newAfm);
            } catch (IdNotAvailableException ex) {
                System.out.println(ex.getMessage());
            } catch (UserNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void deleteUserById() {
        ArrayList<User> users = userService.findAllUsers();
        if (userService.findAllUsers().isEmpty()) {
            System.out.println("There are mo users registred!");
        } else {
            for (User user : users) {
                System.out.println(user.toString());
            }
            System.out.println("Enter the afm number of the user you want to delete below");
            int afm = this.validScannedAfm();
            try {
                userService.deleteUserById(afm);
                System.out.println("User with afm:" + afm + " was deleted!");
            } catch (VehicleNotFoundException ex) {
                System.out.println(ex.getMessage());
            } catch (RepairNotFoundException ex) {
                System.out.println(ex.getMessage());
            } catch (UserNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void getAllUsers() {
        ArrayList<User> users = userService.findAllUsers();
        if (users.isEmpty()) {
            System.out.println("There are no users registered!");
        } else {
            for (User user : users) {
                System.out.println(user.toString());
            }
        }
    }

    public void findAllCarsByUser() {
        ArrayList<User> users = userService.findAllUsers();
        if (userService.findAllUsers().isEmpty()) {
            System.out.println("There are mo users registred!");
        } else {
            for (User user : users) {
                System.out.println(user.toString());
            }
            Set<Integer> vehicles = new TreeSet<Integer>();
            System.out.println("Enter the afm of the User you want to choose");
            int selectedAfm = this.validScannedAfm();
            try {
                vehicles = userService.findVehiclesByUser(selectedAfm);
            } catch (UserNotFoundException ex) {
                System.out.println(ex.toString());
            }
            if (vehicles.isEmpty()) {
                System.out.println("This user has no cars registered to him!");
            } else {
                System.out.println("Users cars");
                for (Integer vehicleId : vehicles) {
                    try {
                        Vehicle vehicle = vehicleService.searchById(vehicleId);
                        System.out.println(vehicle.toString());
                    } catch (VehicleNotFoundException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
    }

    public void findAllCarRepairsByUser() {
        if (userService.findAllUsers().isEmpty()) {
            System.out.println("There are mo users registred!");
        } else {
            Set<Integer> vehicles = new TreeSet<Integer>();
            Set<Integer> repairs = new TreeSet<Integer>();
            try {
                System.out.println("Enter the afm of the User you want to choose");
                int selectedAfm = this.validScannedAfm();
                User selected = userService.searchByAfm(selectedAfm);
                vehicles = userService.findVehiclesByUser(selectedAfm);
                System.out.println(selected.toString() + " was seelcted");
            } catch (UserNotFoundException ex) {
                System.out.println(ex.toString());
            }
            if (vehicles.isEmpty()) {
                System.out.println("This user has no cars registered to him!");
            } else {
                System.out.println(" cars");
                for (Integer vehicleId : vehicles) {
                    try {
                        Vehicle vehicle = vehicleService.searchById(vehicleId);
                        System.out.println(vehicle.toString());
                        repairs = repairService.searchRepairbyVehicleId(vehicleId);
                        if (repairs.isEmpty()) {
                            System.out.println(vehicle.toString() + " has no repairs");
                        } else {
                            for (Integer repairId : repairs) {
                                System.out.println("Repairs of " + vehicle.toString());
                                try {
                                    Repair repair = repairService.searchRepairbyId(repairId);
                                    System.out.println(repair.toString());
                                } catch (RepairNotFoundException ex) {
                                    System.out.println(ex.toString());
                                }
                            }
                        }
                    } catch (VehicleNotFoundException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
    }

    @Override
    public void selector() throws Exception {
        int selection = this.printMenu(USER_MENU, USER_MENU_TITLE);
        switch (selection) {
            case 1: {
                createUser();
                this.selector();
            }
            break;
            case 2:
                findUseById();
                selector();
                break;
            case 3:
                updateUserById();
                selector();
                break;
            case 4:
                deleteUserById();
                selector();
                break;
            case 5:
                getAllUsers();
                selector();
                break;
            case 6:
                findAllCarsByUser();
                selector();
                break;
            case 7:
                findAllCarRepairsByUser();
                selector();
                break;
            case 0:
                CentralController centralMenu = new CentralController();
                centralMenu.printMenu(CentralController.getCENTRAL_MENU(), CentralController.getCENTRAL_MENU_TITLE());
                break;

        }
    }

}
