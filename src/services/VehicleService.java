/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import dao.RepairDaoImp;
import dao.UserDaoImp;
import dao.VehicleDaoImp;
import domain.Vehicle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 *
 * @author User
 */
public class VehicleService {

    VehicleDaoImp vehicleDao = VehicleDaoImp.getInstance();
    RepairDaoImp repairDao = RepairDaoImp.getInstance();
    UserDaoImp userDao = UserDaoImp.getInstance();

    public Vehicle createVehicle(int Id, String model, Date creationDate, String color, int price, int ownerID, String brand) throws UserNotFoundException, IdNotAvailableException {
        ArrayList<Vehicle> vehicles = vehicleDao.getVehicles();
        if (vehicleDao.searchById(Id) != null) {
            throw new IdNotAvailableException();
        }
        UserService helper = new UserService();
        if (helper.searchByAfm(ownerID) == null) {
            throw new UserNotFoundException();
        } else {
            return vehicleDao.createVehicle(Id, model, creationDate, color, price, ownerID, brand);
        }
    }

    public ArrayList<Vehicle> findAllVehicle() {
        ArrayList<Vehicle> Allvehicles = vehicleDao.getVehicles();
        return Allvehicles;
    }

    public Vehicle updateVehcileId(int id1, int id2) throws IdNotAvailableException, VehicleNotFoundException {

        if (vehicleDao.searchById(id1) == null) {
            if (this.searchById(id2) == null) {
                Set<Integer> carRepairs = repairDao.searchRepairbyVehicleId(id1);
                if (carRepairs.isEmpty() == false) {
                    for (int l : carRepairs) {
                        repairDao.searchRepairbyId(l).setVehicleId(id2);
                    }
                }
                vehicleDao.updateVehcileId(id1, id2);
                return vehicleDao.searchById(id2);
            } else {
                throw new IdNotAvailableException();
            }
        } else {
            throw new VehicleNotFoundException();
        }
    }

    public void deleteVehicleById(int id) throws RepairNotFoundException, VehicleNotFoundException {
        if (this.searchById(id) != null) {
            Set<Integer> carRepairs = repairDao.searchRepairbyVehicleId(id);
            if (carRepairs.isEmpty() == false) {
                for (int l : carRepairs) {
                    repairDao.deleteRepairbyId(l);
                }
            }
            ArrayList<Vehicle> vehicles = vehicleDao.getVehicles();
            vehicles.removeIf(vehicle -> vehicle.getId() == id);
        } else {
            throw new VehicleNotFoundException();
        }
    }

    public Set<Integer> VehiclesByOwnerId(int OwnerID) throws UserNotFoundException {
        UserService u = new UserService();
        Set<Integer> garage = new TreeSet<Integer>();
        if (userDao.searchByAfm(OwnerID) == null) {
            throw new UserNotFoundException();
        } else {
            garage = vehicleDao.VehiclesByOwnerId(OwnerID);
            return garage;
        }
    }

    public Vehicle searchById(int Id) throws VehicleNotFoundException {
        ArrayList<Vehicle> AllVehicles = vehicleDao.getVehicles();
        for (Vehicle car : AllVehicles) {
            if (car.getId() == Id) {
                return car;
            }
        }
        throw new VehicleNotFoundException();
    }

}
