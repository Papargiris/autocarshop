/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.EmailNotAvailableException;
import Exceptions.FailedLogInException;
import Exceptions.IdNotAvailableException;
import Exceptions.RepairNotFoundException;
import Exceptions.UserNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.UserTypeEnum;
import com.sun.media.sound.InvalidDataException;
import dao.RepairDaoImp;
import dao.UserDaoImp;
import dao.VehicleDaoImp;
import domain.Repair;
import domain.User;
import domain.Vehicle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Deadpool oles oi ylopoihseis ton methodwn tha ginoun implement apo User
 */
public class UserService {

    UserDaoImp userDao = UserDaoImp.getInstance();
    VehicleDaoImp vehicleDao = VehicleDaoImp.getInstance();
    RepairDaoImp repairDao = RepairDaoImp.getInstance();

    public User createUser(int afm, String Email, String FirstName, String LastName, UserTypeEnum type, String Address, String password) throws IdNotAvailableException, EmailNotAvailableException {
        if (userDao.searchByAfm(afm) != null) {
            throw new IdNotAvailableException();
        } else if (userDao.searchByMail(Email) != null) {
            throw new EmailNotAvailableException();
        } else {
            User dummie = new User(afm, Email, FirstName, LastName, type, Address, password);
            UserDaoImp users = UserDaoImp.getInstance();
            ArrayList<User> Allusers = findAllUsers();
            Allusers.add(dummie);
            return dummie;
        }
    }

    public ArrayList<User> findAllUsers() {
        ArrayList<User> Allusers = userDao.getUsers();
        return Allusers;
    }

    public User searchByMail(String Mail) throws UserNotFoundException {
        ArrayList<User> Allusers = findAllUsers();
        for (User user : Allusers) {
            if (user.getEmail().equals(Mail)) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    public User searchByAfm(int afm) throws UserNotFoundException {
        if (userDao.searchByAfm(afm) != null) {
            return userDao.searchByAfm(afm);
        } else {
            throw new UserNotFoundException();
        }
    }

    public User searchByFirstName(String FirstName) throws UserNotFoundException {
        if (userDao.searchByFirstName(FirstName) != null) {
            return userDao.searchByFirstName(FirstName);
        } else {
            throw new UserNotFoundException();
        }
    }

    public User logIn(String email, String password) {
        ArrayList<User> Allusers = findAllUsers();
        for (User user : Allusers) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public User logInValid(String email, String password) throws FailedLogInException {
        if (logIn(email, password) != null) {
            return logIn(email, password);
        } else {
            throw new FailedLogInException();
        }
    }

    public User updateUserId(int afm, int afm1) throws IdNotAvailableException, UserNotFoundException {
        ArrayList<User> Allusers = userDao.getUsers();
        if (userDao.searchByAfm(afm) != null) {
            if (userDao.searchByAfm(afm1) == null) {
                for (User user : Allusers) {
                    if (user.getAfm() == afm) {
                        if (userDao.userHasCars(afm)) {
                            Set<Integer> maWhips = userDao.findVehiclesByUser(afm);
                            for (int i : maWhips) {
                                vehicleDao.searchById(i).setOwner(afm1);
                            }
                        }
                        user.setAfm(afm1);
                        return user;
                    }
                }
            } else {
                throw new IdNotAvailableException();
            }
        } else {
            throw new UserNotFoundException();
        }
        return null;
    }

    public void deleteUserById(int afm) throws VehicleNotFoundException, RepairNotFoundException, UserNotFoundException {
        if (userDao.searchByAfm(afm) != null) {
            if (userDao.userHasCars(afm)) {
                Set<Integer> maWhips = userDao.findVehiclesByUser(afm);
                for (int i : maWhips) {
                    Set<Integer> carRepairs = repairDao.searchRepairbyVehicleId(i);
                    if (carRepairs.isEmpty() == false) {
                        for (int l : carRepairs) {
                            repairDao.deleteRepairbyId(l);
                        }
                    }
                    vehicleDao.deleteVehicleById(i);
                }
            }
            ArrayList<User> users = findAllUsers();
            users.removeIf(user -> user.getAfm() == afm);
        } else {
            throw new UserNotFoundException();
        }
    }

    public Set<Integer> findVehiclesByUser(int afm) throws UserNotFoundException {
        if (userDao.searchByAfm(afm) != null) {
            return userDao.findVehiclesByUser(afm);
        } else {
            throw new UserNotFoundException();
        }
    }

    public ArrayList<Integer> usersCarsRepairs(int userId) throws UserNotFoundException, VehicleNotFoundException {
        Set<Integer> carRepairs = new TreeSet<Integer>();
        ArrayList<Integer> allRepairs = new ArrayList<Integer>();
        if (userDao.searchByAfm(userId) != null) {
            if (userDao.userHasCars(userId) == true) {
                Set<Integer> garage = userDao.findVehiclesByUser(userId);
                for (int i : garage) {
                    carRepairs = repairDao.searchRepairbyVehicleId(i);
                }
                allRepairs.addAll(garage);
                return allRepairs;
            }
        } else {
            throw new UserNotFoundException();
        }
        return allRepairs;
    }

}
