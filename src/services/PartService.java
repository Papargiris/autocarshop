/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.IdNotAvailableException;
import Exceptions.PartNotFoundException;
import dao.PartDaoImp;
import domain.Part;
import domain.PartTypeEnum;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author User
 */
public class PartService {

    PartDaoImp partDao = PartDaoImp.getInstance();

    public Part createPart(int id, String name, PartTypeEnum type, float cost) throws IdNotAvailableException, PartNotFoundException {

        if (this.searchByPartId(id) == null) {
            Part dummie = new Part(id, name, type, cost);
            ArrayList<Part> AllParts = partDao.getParts();
            AllParts.add(dummie);
            return dummie;
        } else {
            throw new IdNotAvailableException();
        }
    }

    public ArrayList<Part> findAllParts() {
        ArrayList<Part> parts = partDao.getParts();
        return parts;
    }

    public Part updatePartId(int partId, int newPartId) throws IdNotAvailableException, PartNotFoundException {
        
        ArrayList<Part> Allparts = partDao.getParts();
        if (this.searchByPartId(partId) == null) {
            for (Part part : Allparts) {
                if (part.getId() == partId) {
                    part.setId(newPartId);
                    return part;
                }
            }
        } else {
            throw new IdNotAvailableException();
        }
        return null;
    }

    public void deletePart(int partId) throws PartNotFoundException {
        if (searchByPartId(partId) != null) {
            ArrayList<Part> parts = partDao.getParts();
            parts.removeIf(part -> part.getId() == partId);
        } else {
            throw new PartNotFoundException();
        }
    }

    public Part searchByPartId(int id) throws PartNotFoundException {
        ArrayList<Part> Allparts = partDao.getParts();
        for (Part p : Allparts) {
            if (p.getId() == id) {
                return p;
            }
        }
        throw new PartNotFoundException();
        
    }

}
