/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.IdNotAvailableException;
import Exceptions.InvalidDateException;
import Exceptions.RepairNotFoundException;
import Exceptions.VehicleNotFoundException;
import domain.RepairTypeEnum;
import dao.RepairDaoImp;
import dao.VehicleDaoImp;
import domain.Part;
import domain.Repair;
import domain.Vehicle;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author User
 */
public class RepairService {

    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    static final Date OPENINGDATE = parseDate("2004-02-23");

    RepairDaoImp repairDao = RepairDaoImp.getInstance();

    public void createRepair(int id, RepairTypeEnum Status, int vehicleId, float cost) throws RepairNotFoundException, IdNotAvailableException, VehicleNotFoundException {
        VehicleService v = new VehicleService();
        if (this.searchRepairbyId(id) != null) {
            throw new IdNotAvailableException();
        }
        if (v.searchById(vehicleId) != null) {
            if (searchRepairbyId(id) == null) {
                Date repairDate = new Date();
                Repair dummie = new Repair(id, Status, vehicleId, cost, repairDate);
                ArrayList<Repair> AllRepairs = repairDao.getRepairs();
                AllRepairs.add(dummie);
            }
        } else {
            throw new VehicleNotFoundException();
        }
    }

    public ArrayList<Repair> findAllRepairs() {
        ArrayList<Repair> repairs = repairDao.getRepairs();
        return repairs;
    }

    public Set<Integer> searchRepairbyVehicleId(int Vehicleid) throws VehicleNotFoundException {
        VehicleService v = new VehicleService();
        Set<Integer> carRepairs = new TreeSet<Integer>();
        if (v.searchById(Vehicleid) == null) {
            throw new VehicleNotFoundException();
        } else {
            ArrayList<Repair> repairs = repairDao.getRepairs();
            if (repairs.isEmpty()) {
                return carRepairs;
            } else {
                for (Repair repair : repairs) {
                    if (repair.getVehicleId() == Vehicleid) {
                        int rID = repair.getId();
                        carRepairs.add(rID);
                    }
                }
            }
        }
        return carRepairs;
    }

    public Set<Integer> searchRepairByDate(Date date) throws InvalidDateException {
        Set<Integer> carRepairs = new TreeSet<Integer>();
        ArrayList<Repair> repairs = repairDao.getRepairs();
        Date today = new Date();
        if (date.before(date) && date.after(OPENINGDATE)) {
            for (Repair repair : repairs) {
                if (repair.getRepairDate() == date) {
                    int rID = repair.getId();
                    carRepairs.add(rID);
                }
            }
        } else {
            throw new InvalidDateException();
        }
        return carRepairs;
    }

    public void deleteRepairbyId(int id) throws IdNotAvailableException, RepairNotFoundException {
        if (searchRepairbyId(id) != null) {
            ArrayList<Repair> repairs = repairDao.getRepairs();
            repairs.removeIf(repair -> repair.getId() == id);
        } else {
            throw new IdNotAvailableException();
        }
    }

    public float totalRepairCost(int id) throws RepairNotFoundException {
        PartService part = new PartService();
        int totalCost = 0;
        Repair r = searchRepairbyId(id);
        if (r != null) {
            ArrayList<Integer> parts = r.getRepairParts();
            for (Integer p : parts) {
                totalCost += part.searchByPartId(p).getCost();
            }
            float i = r.getCost();
            float finalCost = totalCost + i;
        } else {
            throw new RepairNotFoundException();
        }
        return totalCost;
    }

    public Repair searchRepairbyId(int id) throws RepairNotFoundException {
        ArrayList<Repair> Allrepairs = repairDao.getRepairs();
        for (Repair r : Allrepairs) {
            if (r.getId() == id) {
                return r;
            }
        }
        throw new RepairNotFoundException();
    }

}
